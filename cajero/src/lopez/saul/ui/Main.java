package lopez.saul.ui;
import lopez.saul.dl.Data;
import lopez.saul.bl.Cliente;
import lopez.saul.bl.Cuenta;

import java.io.*;
import java.sql.Date;
import java.util.ArrayList;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    static Data datos = new Data();
    public static void main(String args[]) throws java.io.IOException {
        int opcion = -1;
        do {
            opcion = mostrarMenu();
            procesarOpcion(opcion);
        } while (opcion != 0);

    }

    static void procesarOpcion(int pOpcion) throws java.io.IOException {
        switch (pOpcion) {
            case 1:
                registrarCliente();
                break;
            case 2:
                agregarCuenta();
                break;
            case 3:
                listarClientes();
                break;
            case 4:
                realizarDeposito();
                break;
            case 5:
                realizarRetiro();
                break;
            case 6:
                consultarCuenta();
                break;
            case 0:
                out.println("Fin del programa !!");
                break;
            default:
                out.println("Opcion inválida");
        }
    }

    static void registrarCliente() throws java.io.IOException {
        ArrayList<Cliente> clientes = datos.listarClientes();
        out.println("Ingrese el nombre del cliente");
        String nombre = in.readLine();

        boolean clienteCorrecto = false;
        String id;
        do {
            out.println("Ingrese la identificación del cliente");
            id = in.readLine();
            if (clientes != null) {
                for(int i = 0;i<clientes.size();i++){
                    String identificacionActual = clientes.get(i).getIdentificacion();
                    if (!identificacionActual.equals(id)) {
                        clienteCorrecto = true;
                    } else {
                        out.println("El cliente ya existe, ingrese un ID valido");
                    }
                }
            } else {
                clienteCorrecto = true;
            }
        } while (clienteCorrecto == false);

        out.println("Ingrese la fecha de nacimiento del cliente con el formato yyyy-mm-dd");
        Date fechaNacimiento = Date.valueOf(in.readLine());
        out.println("Ingrese la edad del cliente");
        int edad = Integer.parseInt(in.readLine());
        out.println("Ingrese la direccion del cliente");
        String direccion = in.readLine();

        datos.agregarCliente(nombre,id, fechaNacimiento, edad, direccion);
    }

    static void agregarCuenta() throws java.io.IOException{
        listarClientes();
        out.println("Seleccione un cliente, ejemplo 1");
        int cliente = Integer.parseInt(in.readLine());
        ArrayList<Cuenta> cuentas = datos.listarCuentas(cliente);

        boolean numeroCorrecto = false;
        boolean clienteCorrecto = false;
        String numero;
        do {
            out.println("Ingrese el numero de la cuenta");
            numero = in.readLine();
            if (numero.length() == 7) {
                numeroCorrecto = true;
            } else {
                out.println("Longitud de numero incorrecto, debe ser de 7 digitos");
            }
            if (cuentas != null) {
                for(int i = 0;i<cuentas.size();i++){
                    String cuentaActual = cuentas.get(i).getNumero();
                    if (!cuentaActual.equals(numero)) {
                        clienteCorrecto = true;
                    } else {
                        out.println("La cuenta ya existe, ingrese un nuevo numero");
                    }
                }
            } else {
                clienteCorrecto = true;
            }
        } while (numeroCorrecto == false && clienteCorrecto == false);

        boolean depositoCorrecto = false;
        int deposito;
        do {
            out.println("Ingrese el monto del deposito inicial");
            deposito = Integer.parseInt(in.readLine());
            if (deposito >= 50000) {
                depositoCorrecto = true;
                datos.agregarCuenta(cliente, numero, deposito);
            } else {
                out.println("monto incorrecto, debe ser mayor a 50000");
            }
        } while (depositoCorrecto == false);
    }

    static void listarClientes(){
        out.println("-- lista de clientes");
        ArrayList<Cliente> clientes = datos.listarClientes();

        for(int i = 0;i<clientes.size();i++){
            out.println(i + " - " + clientes.get(i).toString());
        }
    }

    static void listarCuentas(int cliente) {
        out.println("-- lista de cuentas");
        ArrayList<Cuenta> cuentas = datos.listarCuentas(cliente);

        for(int i = 0;i<cuentas.size();i++){
            out.println(i + " - " + cuentas.get(i).toString());
        }
    }

    static void realizarDeposito() throws java.io.IOException{
        listarClientes();
        out.println("Seleccione un cliente, ejemplo 1");
        int cliente = Integer.parseInt(in.readLine());
        listarCuentas(cliente);
        out.println("Seleccione un cliente, ejemplo 1");
        int cuenta = Integer.parseInt(in.readLine());

        boolean montoCorrecto = false;
        int deposito;
        do {
            out.println("Ingrese el monto del deposito");
            deposito = Integer.parseInt(in.readLine());
            if (deposito > 0) {
                montoCorrecto = true;
                datos.realizarDeposito(cliente, cuenta, deposito);
            } else {
                out.println("monto incorrecto, debe ser mayor a 0");
            }
        } while (montoCorrecto == false);
    }

    static void realizarRetiro() throws java.io.IOException{
        listarClientes();
        out.println("Seleccione un cliente, ejemplo 1");
        int cliente = Integer.parseInt(in.readLine());
        listarCuentas(cliente);
        out.println("Seleccione un cliente, ejemplo 1");
        int cuenta = Integer.parseInt(in.readLine());

        boolean montoCorrecto = false;
        int retiro;
        do {
            out.println("Ingrese el monto del deposito");
            retiro = Integer.parseInt(in.readLine());
            if (retiro > 0) {
                montoCorrecto = true;
                datos.realizarRetiro(cliente, cuenta, retiro);
            } else {
                out.println("monto incorrecto, debe ser mayor a 0");
            }
        } while (montoCorrecto == false);
    }

    static void consultarCuenta() throws java.io.IOException{
        listarClientes();
        out.println("Seleccione un cliente, ejemplo 1");
        int cliente = Integer.parseInt(in.readLine());
        listarCuentas(cliente);
        out.println("Seleccione un cliente, ejemplo 1");
        int cuenta = Integer.parseInt(in.readLine());

        Cuenta cuentaActual = datos.listarCuentas(cliente).get(cuenta);
        int saldoActual = cuentaActual.getSaldo();

        if (saldoActual > 0) {
            out.println(saldoActual);
        } else {
            out.println("Cuenta no tiene fondos");
        }
    }

    static int mostrarMenu() throws java.io.IOException {
        int opcion;
        opcion = 0;
        out.println("1. Registrar cliente");
        out.println("2. Agregar cuenta");
        out.println("3. Listar clientes");
        out.println("4. Realizar deposito");
        out.println("5. Realizar retiro");
        out.println("6. Mostrar saldo de cuenta");
        out.println("0. Salir del sistema");
        out.println("Digite la opción");
        opcion = Integer.parseInt(in.readLine());
        return opcion;
    }
}
