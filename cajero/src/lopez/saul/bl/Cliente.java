package lopez.saul.bl;

import java.sql.Date;
import java.util.ArrayList;

public class Cliente {
    private String nombre;
    private String identificacion;
    private Date fechaNacimiento;
    private int edad;
    private String direccion;
    private ArrayList<Cuenta> cuentas = new ArrayList<>();

    public Cliente() { }

    public Cliente(String nombre, String identificacion, Date fechaNacimiento, int edad, String direccion) {
        setNombre(nombre);
        setIdentificacion(identificacion);
        setFechaNacimiento(fechaNacimiento);
        setEdad(edad);
        setDireccion(direccion);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public void agregarCuentas(Cuenta cuenta) {
        cuentas.add(cuenta);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", direccion='" + direccion + '\'' +
                ", cuenta=" + cuentas +
                '}';
    }
}
