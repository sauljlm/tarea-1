package lopez.saul.dl;

import lopez.saul.bl.Cliente;
import lopez.saul.bl.Cuenta;

import java.sql.Date;
import java.util.ArrayList;

public class Data {
    private ArrayList<Cliente> clientes;

    public Data(){
    }

    public void agregarCliente(String nombre, String id, Date fechaNacimiento, int edad, String direccion){
        Cliente empleado = new Cliente();
        empleado.setNombre(nombre);
        empleado.setIdentificacion(id);
        empleado.setFechaNacimiento(fechaNacimiento);
        empleado.setEdad(edad);
        empleado.setDireccion(direccion);

        if(clientes == null){
            clientes = new ArrayList<>();
        }

        clientes.add(empleado);
    }

    public void agregarCuenta(int cliente, String numero, int deposito) {
        Cuenta nuevaCuenta = new Cuenta(numero, deposito);
        clientes.get(cliente).agregarCuentas(nuevaCuenta);
    }

    public void realizarDeposito(int cliente, int cuenta, int cantidad){
        Cliente clienteActual = clientes.get(cliente);
        Cuenta cuentaActual =  clienteActual.getCuentas().get(cuenta);
        int saldo = cuentaActual.getSaldo();

        cuentaActual.setSaldo(saldo + cantidad);
    }

    public void realizarRetiro(int cliente, int cuenta, int cantidad){
        Cliente clienteActual = clientes.get(cliente);
        Cuenta cuentaActual =  clienteActual.getCuentas().get(cuenta);
        int saldo = cuentaActual.getSaldo();

        if (saldo >= cantidad) {
                cuentaActual.setSaldo(saldo - cantidad);
        } else {
            System.out.println("La cuenta no tiene los fondos suficientes");
        }
    }

    public ArrayList<Cliente> listarClientes(){
        return  clientes;
    }

    public ArrayList<Cuenta> listarCuentas(int cliente){
        Cliente clienteActual = clientes.get(cliente);
        return  clienteActual.getCuentas();
    }

}
